//
//  HomeViewController.swift
//  CalculateBanknote
//
//  Created by Mac on 13/5/2564 BE.
//

import UIKit

class HomeViewController: UIViewController,CustomSegmentedControlViewDelegate {
    var homeVM = HomeViewModel()
    
    @IBOutlet weak var contentView: UIView!
    
    @IBAction func slideMenuAction(_ sender: Any) {
        revealViewController()?.revealSideMenu()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        sideMenuBtn.target = revealViewController()
//        sideMenuBtn.action = #selector(revealViewController()?.revealSideMenu)
        self.contentView.layer.layoutIfNeeded()
        let codeSegmented = CustomSegmentedControlView(frame: CGRect(x: 0, y: 0, width: self.contentView.frame.width, height: 50), buttonTitle: ["ข้อมูลส่วนตัว","สถิติเวลาทำงาน"])
        codeSegmented.backgroundColor = .clear
        codeSegmented.delegate = self
        contentView.addSubview(codeSegmented)
        
        
    }
    
    func changToindax(index: Int) {
        homeVM.showViewOfControSagement(targetView: self.contentView, index:index, height: 50)
        print("Index = \(index)")
    }
    
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
