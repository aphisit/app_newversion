//
//  SMSlideMenuViewController.swift
//  CalculateBanknote
//
//  Created by Mac on 12/5/2564 BE.
//

import UIKit
protocol SideMenuViewControllerDelegate {
    func selectedCell(_ storyboardIdentifier: String)
}
class SMSlideMenuViewController: UIViewController {
    var delegate: SideMenuViewControllerDelegate?
    var statusShowMenu:[Any] = []
    var nameArray:[String] = []
    var isExpandedArray:[Bool] = []
    var rowArray:[Int] = []
    var dataArray:NSArray = []
    
    
    
    
    @IBOutlet var headerImageView: UIImageView!
    @IBOutlet var sideMenuTableView: UITableView!
    @IBOutlet var footerLabel: UILabel!
    var defaultHighlightedCell: Int = 0
    var menu : [SideMenuModel] = [
        SideMenuModel(icon: UIImage(systemName: "house.fill")!, title: "Home"),
        SideMenuModel(icon: UIImage(systemName: "music.note")!, title: "Music"),
        SideMenuModel(icon: UIImage(systemName: "film.fill")!, title: "Movies"),
        SideMenuModel(icon: UIImage(systemName: "book.fill")!, title: "Books"),
        SideMenuModel(icon: UIImage(systemName: "person.fill")!, title: "Profile"),
        SideMenuModel(icon: UIImage(systemName: "slider.horizontal.3")!, title: "Settings"),
        SideMenuModel(icon: UIImage(systemName: "hand.thumbsup.fill")!, title: "Like us on facebook")
    ]

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.sideMenuTableView.delegate = self
        self.sideMenuTableView.dataSource = self
        self.sideMenuTableView.backgroundColor = #colorLiteral(red: 0.737254902, green: 0.1294117647, blue: 0.2941176471, alpha: 1)
        self.sideMenuTableView.separatorStyle = .none
        
        // Set Highlighted Cell
                DispatchQueue.main.async {
                    let defaultRow = IndexPath(row: self.defaultHighlightedCell, section: 0)
                    self.sideMenuTableView.selectRow(at: defaultRow, animated: false, scrollPosition: .none)
                }
        // Footer
        self.footerLabel.textColor = UIColor.white
        self.footerLabel.font = UIFont.systemFont(ofSize: 12, weight: .bold)
        self.footerLabel.text = "Developed by John Codeos"
        self.readMenuList()
        // Register TableView Cell
        self.sideMenuTableView.register(SlideMenuTableViewCell.nib, forCellReuseIdentifier: SlideMenuTableViewCell.identifier)
        // Update TableView with the data
       // self.sideMenuTableView.reloadData()
        
    }
    
    func readMenuList() -> Void {
        

        //var myEnglishArray:[NSArray] = []
        let path:String? = Bundle.main.path(forResource: "MenuList", ofType: "plist")
                
//        var numbers = [1, 2, 3, 4, 5]
//        numbers.append(100)
//        print(numbers)
        dataArray = NSArray(contentsOfFile: path!)!
        if let arry = NSArray(contentsOfFile: path!){
            
            for i in 0...arry.count-1 {
                let arr:NSArray = arry[i] as! NSArray
                
                for j in 0...arr.count-1 {
                    let dic:NSDictionary = arr.object(at: j) as! NSDictionary
                    
                    if dic.value(forKey: "cellIdentifier") as! String == "mainNavCell" {
                        isExpandedArray.append(dic.value(forKey: "isExpanded") as! Bool)
                        nameArray.append(dic.value(forKey:"name") as! String)
                        rowArray.append(dic.value(forKey: "amount_row") as! Int)
                    }
                    
            
                }
            }
            
        }
        sideMenuTableView.reloadData();
    }
    
   

}

extension SMSlideMenuViewController:UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return 44
    }
}
extension SMSlideMenuViewController:UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return isExpandedArray.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return self.nameArray.count
        print("section = \(section)")
        if isExpandedArray[section] {
            return rowArray[section]
        }else{
            return 1
        }
        
          
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           guard let cell = sideMenuTableView.dequeueReusableCell(withIdentifier: SlideMenuTableViewCell.identifier, for: indexPath) as? SlideMenuTableViewCell else { fatalError("xib doesn't exist") }
        print("section = \(indexPath.section)")
        print("row = \(indexPath.row)")
        
        cell.iconImageView.image = self.menu[indexPath.section].icon
           //cell.titleLabel.text = self.menu[indexPath.row].title
        cell.titleLabel.text = self.nameArray[indexPath.section]

           // Highlighted color
           let myCustomSelectionColorView = UIView()
           myCustomSelectionColorView.backgroundColor = #colorLiteral(red: 0.6196078431, green: 0.1098039216, blue: 0.2509803922, alpha: 1)
           cell.selectedBackgroundView = myCustomSelectionColorView
           return cell
    }

       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let currentSectionCells = getCellDescriptorForIndexPath(indexPath: indexPath)
        
        if currentSectionCells.value(forKey: "cellIdentifier") as! String == "mainNavCell" {
            print("xxx")
        }else{
            self.delegate?.selectedCell(currentSectionCells.value(forKey: "storyboardIdentifier") as! String)
        }
        
        print("isExpanded = \(indexPath.section)")
        if isExpandedArray[indexPath.section] == true {
            isExpandedArray[indexPath.section] = false
        }else{
            isExpandedArray[indexPath.section] = true
        }
        tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
           
     
           // ...
            //self.delegate?.selectedCell(indexPath.row)
           // Remove highlighted color when you press the 'Profile' and 'Like us on facebook' cell
//           if indexPath.row == 4 || indexPath.row == 6 {
//               tableView.deselectRow(at: indexPath, animated: true)
//           }
       }
    
    func getCellDescriptorForIndexPath(indexPath: IndexPath) -> NSDictionary {
        print("section = \(indexPath.section)")
        print("row = \(indexPath.row)")
        let data = dataArray[indexPath.section] as! NSArray
        let dataDic = data[indexPath.row] as! NSDictionary
        
        return dataDic
        
    }
}
