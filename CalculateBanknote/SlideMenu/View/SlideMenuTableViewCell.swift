//
//  SlideMenuTableViewCell.swift
//  CalculateBanknote
//
//  Created by Mac on 12/5/2564 BE.
//

import UIKit
struct SideMenuModel {
    var icon: UIImage
    var title: String
}
class SlideMenuTableViewCell: UITableViewCell {
    class var identifier: String { return String(describing: self) }
    class var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        // Background
        self.backgroundColor = .clear
                
        // Icon
        self.iconImageView.tintColor = .white
                
        // Title
        self.titleLabel.textColor = .white
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
