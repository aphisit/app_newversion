//
//  ListBankTableViewCell.swift
//  CalculateBanknote
//
//  Created by Mac on 9/4/2564 BE.
//

import UIKit

class ListBankTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var bankLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
