//
//  CalculateViewController.swift
//  CalculateBanknote
//
//  Created by Mac on 30/3/2564 BE.
//

import UIKit

class CalculateViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, CalculateViewModelDelegate {
    var bank = ["1000","500","100","50","20","10","5"]
    var amountArray:Array<Any> = []
    
    let calculateViewModel = CalculateViewModel()
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var amountTextField: UITextField!
    @IBAction func calculateAction(_ sender: Any) {
        let vc = CalculateViewModel()
        vc.delegate = self
        vc.calculate(price: Int(priceTextField.text ?? "") ?? 0, typeBank: Int(amountTextField.text ?? "") ?? 0)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "ListBankTableViewCell", bundle: nil), forCellReuseIdentifier: "CustomCell")
        self.tableView.delegate = self
        self.tableView.dataSource = self
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return amountArray.count; 
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomCell", for: indexPath) as! ListBankTableViewCell
        cell.bankLabel.text = bank[indexPath.row]
        cell.amountLabel.text = "\(amountArray[indexPath.row])"
        return cell
    }
    
    func successData(_ controller: CalculateViewModel, text: String, bankArray: Array<Int>) {
        if text == "YES" {
            amountArray = bankArray
        }
        tableView.reloadData()
    }
    
}
